<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AssetStatusOptions extends Model
{
    //
    protected $table = 'assetStatusOptions';
    public $timestamps = false;
}
