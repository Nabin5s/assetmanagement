<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AssetManufacturers extends Model
{
    //
    protected $table = 'assetManufacturers';
    public $timestamps = false;
}
