<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AssetModels extends Model
{
    //
    protected $table = 'assetModels';
    public $timestamps = false;

    public function aceVersions()
    {
        return $this->hasOne('App\Model\AssetsManagement\AceVersions', 'id', 'ace_version');
    }
    public function assetManufacturers()
    {
        return $this->hasOne('App\Model\AssetsManagement\AssetManufacturers', 'id', 'manufacturer_id');
    }
    public function assetTypes()
    {
        return $this->hasOne('App\Model\AssetsManagement\AssetTypes', 'id', 'asset_type');
    }
}
