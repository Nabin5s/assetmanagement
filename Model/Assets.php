<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    protected $with = ['model', 'location', 'notes'];
    protected $table = 'assets';
    public $timestamps = false;


    public function model()
    {
        return $this->hasOne('App\Model\AssetsManagement\AssetModels', 'id', 'model_id');
    }
    public function owner() {
        return $this->morphTo(null, 'owner_type', 'owner_id');
    }
    public function location() {
        return $this->hasOne('App\Model\Rooms', 'id', 'location_id');
    }
    public function notes() { // refer_table 1 means assets table, refer to noteTables
      return $this->hasMany('App\Model\Notes', 'refer_id', 'id')->where('refer_table', 1);
    }
}
