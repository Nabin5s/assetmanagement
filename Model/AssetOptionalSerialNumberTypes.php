<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AssetOptionalSerialNumberTypes extends Model
{
    //
    protected $table = 'assetOptionalSerialNumberTypes';
}
