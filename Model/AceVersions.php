<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AceVersions extends Model
{
    //
    protected $table = 'aceVersions';
    public $timestamps = false;
}
