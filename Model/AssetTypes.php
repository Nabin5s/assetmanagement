<?php

namespace App\Model\AssetsManagement;

use Illuminate\Database\Eloquent\Model;

class AssetTypes extends Model
{
    //
    protected $table = 'assetTypes';
    public $timestamps = false;
}
