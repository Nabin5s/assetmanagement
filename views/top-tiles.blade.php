<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ url('it-assets/all?term=Received') }}">
      <div class="tile-stats">
      <div class="icon"><i class="fa fa-cube"></i></div>
      <div class="count">{{ $ReceivedStatus }}</div>
      <h3>Received Assets</h3>
      <p>Click to see assets that are Boxed or Staged.</p>
    </div>
  </a>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ url('it-assets/all?term=Active') }}">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-desktop"></i></div>
      <div class="count">{{ $ActiveStatus }}</div>
      <h3>Active Assets</h3>
      <p>Click to see assets that are Assigned or Stored.</p>
    </div>
    </a>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ url('it-assets/all?term=Staged') }}">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-truck"></i></div>
      <div class="count">{{ $StagedStatus }}</div>
      <h3>Staged Assets</h3>
      <p>Click to see assets that are Repurposed, Stored or Returned.</p>
    </div>
    </a>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="{{ url('it-assets/all?term=On-hold') }}">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-hdd-o"></i></div>
      <div class="count">{{ $OnHoldStatus }}</div>
      <h3>On-hold Assets</h3>
      <p>Click to see assets that are on-hold.</p>
    </div>
    </a>
  </div>
</div>
<div class="clearfix"></div>
