<!-- move user asset modal content -->
<div class="modal fade" id="moveUserAssetModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Change Asset's Status</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="moveUserAssetForm" action="{{ url('it-assets/move') }}" method="get">
                    {{ csrf_field() }}

                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-round btn-default" data-dismiss="modal"  aria-label="Close">
                Continue
              </button>
            </div>
        </div>
    </div>
</div>
<!-- /move user asset modal content -->

@push('assets_scripts')
    <script type="text/javascript">

    /*
    * If user has assets assigned in past
    * a model is loaded and if past assets'
    * status need to be changed, it can be done
    */
    function updateStatus(serial) {
        var $btn = $('#'+serial);
        $btn.button('loading');
        let status = $btn.parent().siblings('select').val();
        let data = {newStatus: status, changeStatusOf: serial}
        AjaxPostCall( '{{ url('it-assets') }}', 'json', data, function(response) {
            afterStatus.update(response);
        });
        afterStatus = {                                   // functions called after search
           update: function(response) {
             $btn.parent().parent().html(response.success);
           }
         }
    }

    </script>

@endpush
