
<!-- ace version modal content -->
<div class="modal fade" id="addAceVersionModal" tabindex="-1" role="dialog" aria-labelledby="Ace Versions" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Add Ace Version</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="aceVersionsForm" action="{{ url('it-assets') }}" method="post" data-parsley-validate class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Ace Version</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-ace-version"  placeholder="Enter Ace Version number only"  required="required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Description</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-ace-description"  placeholder="Enter a short description" required="required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ace version modal content -->
