@extends('it_assets_management.layout')
@section('assets_container')

<div class="">
  <div class="x_panel">
    <div class="x_title">
      <h2>IT Asset Management Field Settings</h2>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <!-- view all page content -->
    <div class="x_content">
      <div class="col-md-2 col-sm-12 col-xs-12">
        <!-- start accordion -->
        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel">
            <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
              <h4 class="panel-title">Asset Models <span class='fa fa-chevron-down pull-right'></span></h4>
            </a>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                <div class="form-group">
                  <label for="assetModelsSelect">Click on one of the asset models to modify:</label>
                  <select size="10" class="form-control" id="assetModelsSelect">
                    @foreach($assetModels as $key => $assetModel)
                    <option value="{{ $key }}">{{ $assetModel->name . " (ACE " . $assetModel->aceVersions->version_number . ")" }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group collapse">
                  <label for="assetModelsInactiveSelect">Click on one of the inactive asset models to modify:</label>
                  <select size="10" class="form-control red" id="assetModelsInactiveSelect">
                    @foreach($assetModelsInactive as $key => $assetModelInactive)
                    <option value="{{ $key }}">{{ $assetModelInactive->name . " (ACE " . $assetModelInactive->aceVersions->version_number . ")" }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="btn-group" id="assetModelsBtn">
                  <button class="btn btn-primary" type="button">Add New</button>
                  <button class="btn btn-success collapse" type="button">View Active</button>
                  <button class="btn btn-info" type="button">View Inactive</button>
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              <h4 class="panel-title">Asset Types <span class='fa fa-chevron-down pull-right'></span></h4>
            </a>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                <div class="form-group">
                  <label for="assetTypesSelect">Click on one of the asset types to modify:</label>
                  <select size="10" class="form-control" id="assetTypesSelect">
                    @foreach($assetTypes as $key => $assetType)
                    <option value="{{ $key }}">{{ $assetType->type }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group collapse">
                  <label for="assetTypesInactiveSelect">Click on one of the inactive asset types to modify:</label>
                  <select size="10" class="form-control red" id="assetTypesInactiveSelect">
                    @foreach($assetTypesInactive as $key => $assetTypeInactive)
                    <option value="{{ $key }}">{{ $assetTypeInactive->type }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="btn-group" id="assetTypesBtn">
                  <button class="btn btn-primary" type="button">Add New</button>
                  <button class="btn btn-success collapse" type="button">View Active</button>
                  <button class="btn btn-info" type="button">View Inactive</button>
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              <h4 class="panel-title">ACE Versions <span class='fa fa-chevron-down pull-right'></span></h4>
            </a>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                <div class="form-group">
                  <label for="aceVersionsSelect">Click on one of the ace versions to modify:</label>
                  <select size="10" class="form-control" id="aceVersionsSelect">
                    @foreach($aceVersions as $key => $aceVersion)
                    <option value="{{ $key }}" >ACE {{ $aceVersion->version_number }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group collapse">
                  <label for="aceVersionsInactiveSelect">Click on one of the inactive ace versions to modify:</label>
                  <select size="10" class="form-control red" id="aceVersionsInactiveSelect">
                    @foreach($aceVersionsInactive as $key => $aceVersionInactive)
                    <option value="{{ $key }}" >ACE {{ $aceVersionInactive->version_number }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="btn-group" id="aceVersionsBtn">
                  <button class="btn btn-primary" type="button">Add New</button>
                  <button class="btn btn-success collapse" type="button">View Active</button>
                  <button class="btn btn-info" type="button">View Inactive</button>
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <a class="panel-heading collapsed" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
              <h4 class="panel-title">Asset Manufacturers <span class='fa fa-chevron-down pull-right'></span></h4>
            </a>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
              <div class="panel-body">
                <div class="form-group">
                  <label for="assetManufacturersSelect">Click on one of the asset manufacturers to modify:</label>
                  <select size="10" class="form-control" id="assetManufacturersSelect">
                    @foreach($assetManufacturers as $key => $assetManufacturer)
                      <option value="{{ $key }}">{{ $assetManufacturer->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group collapse">
                  <label for="assetManufacturersInactiveSelect">Click on one of the inactive asset manufacturers to modify:</label>
                  <select size="10" class="form-control red" id="assetManufacturersInactiveSelect">
                    @foreach($assetManufacturersInactive as $key => $assetManufacturerInactive)
                      <option value="{{ $key }}">{{ $assetManufacturerInactive->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="btn-group" id="assetManufacturersBtn">
                  <button class="btn btn-primary" type="button">Add New</button>
                  <button class="btn btn-success collapse" type="button">View Active</button>
                  <button class="btn btn-info" type="button">View Inactive</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of accordion -->
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title" id="fieldSettingsTitle">
            <h2>Field Settings Form</h2>
            {{-- <ul class="nav navbar-right panel_toolbox">
                <li><a id="deleteEntity"><i class="fa fa-trash red"></i></a>
                </li>
                <li><a id="restoreEntity"><i class="fa fa-history green"></i></a>
                </li>
            </ul> --}}
            <div class="clearfix"></div>
          </div>
          <!-- start of settings form -->
          <div class="x_content">
            <div id="settingsForm">
              <h4 class="big-screen">Select one of the options from the various asset's fields on left to generate edit form here.</h4>
              <h4 class="small-screen">Select one of the options from the various asset's fields on top to generate edit form here.</h4>
            </div>
            @include('it_assets_management.delete-asset-form')
          </div>
        </div>
        <!-- end of settings form -->
      </div>

    </div>
    <!-- /view all page content -->
  </div>
</div>

@endsection
@push('assets_scripts')
<script type="text/javascript">
var assetState = 'active';
$(document).ready(function(){
  $('#deleteEntity, #restoreEntity').hide();
  $('#accordion select').change(function() {   // selection for particular enitity
    // alert($(this).parent().parent().parent().prev().find('h4').text());
    let formSelector = $(this).attr('id');
    let formId = formSelector.substring(0, (formSelector.length - 6));
    var term = formSelector + '_' + $(this).val();
    // unselect other options
    $('#accordion').find('select').not('#'+formSelector).find('option').prop("selected", false);
    //show loading
    $('#settingsForm').LoadingOverlay('show');
    if (formId.substr(formId.length - 8) == "Inactive"){
      formId = formId.substr(0, formId.length - 8);
      assetState = 'inactive';
    }
    else {
      assetState = 'active';
    }
    form.init(formId);
    AjaxGetCall('{{ url("it-assets/json-settings?term=")}}' + term,'json', true, function(response) {
    // dynamic function call
    $.when(form[formId](response)).done(function(){
      $('#settingsForm').trigger('contentchanged'); // notify about form change
      // hide loading
      $('#settingsForm').LoadingOverlay('hide', true);
    });
    });
  });

  form = {
    init: function(formId) {
      // alert(formId);
      // alert($('#'+formId+'Select').parent().parent().parent().prev().find('h4').text());
      $('#fieldSettingsTitle h2').text($('#'+formId+'Select').parent().parent().parent().prev().find('h4').text() + 'Settings Form');
      $('#deleteEntity, #restoreEntity').hide();
      let formName = '#' + formId + "Form";
      let isModified = '<input type="hidden" name="isModified" value="1" />';

        $('#settingsForm').empty().html('');
        var form = $(formName).clone();
        // alert(form.attr('id'));
        $(form).find('[type="submit"]').text('Update').before(isModified).nextAll().remove();
        $(form).find("option[value='Add New']").remove();
        $(form).appendTo('#settingsForm');

    },
    assetModels: function(response) {
        data = response.assetModels || response.assetModelsInactive;
        console.log(data);
      var newForm = $('#settingsForm > #assetModelsForm');
      $(newForm).find('#assetManufacturers').val(data.manufacturer_id);
      $(newForm).find('#aceVersions').val(data.ace_version);
      $(newForm).find('#assetTypes').val(data.asset_type);
      $(newForm).find('[name="new-model-name"]').val(data.name);
      $(newForm).find('[name="new-model-description"]').val(data.description);
      $(newForm).find('.isComputer').html('<select class="form-control" name="is-computer" required="required">'
      + '<option value="1">Yes</option><option value="0">No</option>');
      $(newForm).find('[name="is-computer"]').val(data.is_computer);
      $(newForm).find('[name="new-memory-capacity"]').val(data.memory_capacity);
      $(newForm).find('[name="isModified"]').val( 'assetModels_' + data.id );
      form.icons('assetModels_' + data.id, response.dependent);

    },
    assetTypes: function(response) {
        data = response.assetTypes || response.assetTypesInactive;
      // console.log(data);
      var newForm = $('#settingsForm > #assetTypesForm');
      $(newForm).find('[name="new-equipment-type"]').val(data.type);
      $(newForm).find('[name="isModified"]').val( 'assetTypes_' + data.id );
      form.icons('assetTypes_' + data.id, response.dependent);
    },
    aceVersions: function(response) {
        data = response.aceVersions || response.aceVersionsInactive;
      // console.log(data);
      var newForm = $('#settingsForm > #aceVersionsForm');
      $(newForm).find('[name="new-ace-version"]').val(data.version_number);
      $(newForm).find('[name="new-ace-description"]').val(data.description);
      $(newForm).find('[name="isModified"]').val( 'aceVersions_' + data.id );
      form.icons('aceVersions_' + data.id, response.dependent);
    },
    assetManufacturers: function(response) {
        data = response.assetManufacturers || response.assetManufacturersInactive;
      // console.log(data);
      var newForm = $('#settingsForm > #assetManufacturersForm');
      $(newForm).find('[name="new-manufacturer-name"]').val(data.name);
      $(newForm).find('[name="isModified"]').val( 'assetManufacturers_' + data.id );
      form.icons('assetManufacturers_' + data.id, response.dependent);
    },
    delete: function(value, dependent) {
      // $('#deleteEntity').show();
      $('#settingsForm').find('[type="submit"]').after('<button type="button" value="'+ dependent +'" id="deleteEntity" class="btn btn-danger">Delete</button>');
      $('[name="isDeleted"]').val( value );
    },
    restore: function(value, dependent) {
      // $('#restoreEntity').show();
      $('#settingsForm').find('[type="submit"]').after('<button type="button" value="'+ dependent +'" id="restoreEntity" class="btn btn-success">Restore</button>');
      $('[name="isRestore"]').val( value );
    },
    icons: function(value, dependent) {
      if(assetState == "active") {
      $('[name="isRestore"]').attr('name', 'isDeleted');
        form.delete(value, dependent);
      }
      else {
        $('[name="isDeleted"]').attr('name', 'isRestore');
        form.restore(value, dependent);
      }
    },
    clear: function() {
      // re-init form
      $('#settingsForm').empty()
      .html('<h4>Select one of the options from the various asset entities tabs to generate edit form here.</h4>');
      $('#fieldSettingsTitle h2').text('Field Settings Form');
      // unselect all
      $('#accordion').find('select').find('option').prop("selected", false);
    }
  }

  $('#settingsForm').bind('contentchanged', function() {
    // do something after the div content has changed
    $('#settingsForm').find('[type="submit"]').click(function(e) {
      e.preventDefault();
      let formId = $(this).parent().parent().parent().attr('id');
      confirmAlert(formId, 'modify');
    });

    $('button#deleteEntity').click(function(e) {
      e.preventDefault();
      console.log(e.target.value);
      //console.log('{{ \App\Model\AssetsManagement\AssetModels::where('ace_version', 4)->count() }}');
      confirmAlert('deleteAssetForm', 'custom', 'Do you want to delete this?',
      '<p>You are about to make changes to '+ $('#fieldSettingsTitle h2').text() +'.</p>'+
        '<p><span class="red">'+e.target.value+' depend on this field.</span> Do you want to proceed?</p>');
    });

    $('button#restoreEntity').click(function(e) {
      e.preventDefault();
      confirmAlert('deleteAssetForm', 'modify');
    });

  }); //end of contentchanged

  $('.btn-group :button').click(function() {

    switch ($(this).text()) {
      case 'Add New':
      form.clear();
        let btnID = $(this).parent().attr('id').capitalizeFirstLetter();
          let modelID = "#add"+btnID.substring(0, (btnID.length - 4) )+"Modal";
          console.log(modelID);
          $(modelID).modal({
              backdrop: 'static',
              keyboard: false
          });
      break;
      case 'View Inactive':
      $( this ).prev().toggleClass( "collapse" );
      $( this ).toggleClass( "collapse" );
      console.log($('#'+$(this).parent().attr('id')).siblings('.collapse'));
      $('#'+$(this).parent().attr('id')).siblings('.collapse').toggleClass( "collapse" ).prev().toggleClass( "collapse" );
      // $(this).parent().parent().find('.form-group').toggleClass( "collapse" );
      break;
      case 'View Active':
      $( this ).next().toggleClass( "collapse" );
      $( this ).toggleClass( "collapse" );
      $('#'+$(this).parent().attr('id')).siblings('.collapse').toggleClass( "collapse" ).next().toggleClass( "collapse" );
      break;
    }
    // console.log($(this).parent().parent().find('form-group collapse'));
  });
}); // jquery ready function


</script>
@endpush
