@extends('layouts.blank')

@push('stylesheets')
  <!-- Switchery -->
<link href="{{ asset("css/switchery.min.css") }}" rel="stylesheet">
<link href="{{ asset("css/assetManagementStyle.css") }}" rel="stylesheet">
@endpush

@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
    <div class="row">
    <!-- top tiles content -->
    @include('it_assets_management.top-tiles')
    <!-- /top tiles content -->
    <!-- app menu content -->
    @include('it_assets_management.menu')
    <!-- /app menu content -->
    @yield('assets_container')
    </div>
    @if( request()->permission == "w" && empty($tableSettings))
      @include('it_assets_management.add-asset-model')
      @include('it_assets_management.add-asset-type')
      @include('it_assets_management.add-ace-version')
      @include('it_assets_management.add-asset-manufacturer')
      @include('it_assets_management.move-user-asset')
      @include('templates.confirmModal')
    @endif
    </div>
    <!-- /page content -->
@endsection

@push('scripts')
    <!-- Switchery -->
    <script src="{{ asset('js/switchery.min.js') }}"></script>
    <script src="{{ asset('js/AjaxFunctions.js') }}"></script>
    @stack('assets_scripts')

    <script>
    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }


    $( function() {
        $(':reset').click( function() {
            window.location = "{{ url(request()->path()) }}";
        });
            $('.aims-update :input, .isComputer :input').change(function() {
                if($(this).is(':checked')){ //if is aims updated
                    $(this).parent().find('label').text('Yes');
                    $(this).val('1');
                }
                else {
                    $(this).parent().find('label').text('No');
                    $(this).val('0');
                }
            });
        $('#aceVersions').change(function() {
            if($('#aceVersions').val() == "Add New"){
                var data = $("#" + $(this).attr('id') + "Form .form-group").clone();
                $(data).find('button').remove();
                $(data).insertAfter($(this).parent().parent());
                $(this).prop('disabled', true);
            }
        });
        $('#assetTypes').change(function() {
            if($('#assetTypes').val() == "Add New"){
                var data = $("#" + $(this).attr('id') + "Form .form-group").clone();
                $(data).find('button').remove();
                $(data).insertAfter($(this).parent().parent());
                $(this).prop('disabled', true);
            }
        });
        $('#assetModel').change(function() {
            if($('#assetModel').val() == "Add New Model") {
                let modelID = "#add"+$(this).attr('id').capitalizeFirstLetter()+"Modal";
                $(modelID).modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        });

        $('#assetManufacturers').change(function() {
            if($(this).val() == "Add New") {
              var data = $("#" + $(this).attr('id') + "Form .form-group").clone();
              $(data).find('button').remove();
              $(data).insertAfter($(this).parent().parent());
              $(this).prop('disabled', true);
            }
        });


      });
      </script>
@endpush
