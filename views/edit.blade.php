
<div class="">
    <div class="x_panel">
          <div class="x_title">
            <h2>IT Asset Management</h2>
            <div class="clearfix"></div>
          </div>
    <!-- asset form content -->
    <div class="x_content">
      <h5>Fill in the form below to add new inventory.</h5>
            <br />
            <form class="form-horizontal" id="assetForm" data-parsley-validate method="post" action="{{ url('it-assets') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Asset Model<span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" id="assetModel" name="asset-model">
                            @if(!$assetModels->contains($editAsset[0]->model_id))
                                <option style="color:red;" value="{{ $editAsset[0]->model->id }}">{{ $editAsset[0]->model->name . " (ACE " . $editAsset[0]->model->aceVersions->version_number . ")" }}</option>
                            @endif
                            @foreach($assetModels as $assetModel)
                            <option value="{{ $assetModel->id }}">{{ $assetModel->name . " (ACE " . $assetModel->aceVersions->version_number . ")" }}</option>
                            @endforeach
                            <option>Add New Model</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="ncedSerial">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">NCED Serial Number<span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" placeholder="Enter 7 digit NCED number here" required="required" name="nced-serial" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Manufacturer Serial Number<span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" placeholder="Enter manufacturer serial number here" required="required" name="manufacturer-serial" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status<span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" id="assetStatus" name="asset-status">
                            @foreach ($assetStatusOptions as $assetStatusPri)
                            @if($assetStatusPri->status_mode === "pri")
                            <optgroup label="{{ $assetStatusPri->value }}">
                                @foreach ($assetStatusOptions as $assetStatusSec)
                                @if(str_contains($assetStatusSec->valid_parents, $assetStatusPri->id))

                                <option value="{{ $assetStatusPri->id }}-{{ $assetStatusSec->id }}">{{ $assetStatusPri->value . '-' .$assetStatusSec->value }}</option>
                                @endif
                                @endforeach
                            </optgroup>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div id="assignmentForm" class="collapse">
                    {{-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Who are you assigning it to?</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="assignment">
                                    <input type="checkbox" class="js-switch" name="assign-to"/> <label>User
                                </label>
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Who are you assigning it to?</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" id="assignment" name="assignment">
                                <option>Location</option>
                                <option>User</option>
                                <option>Group</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group collapse" id="users">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User's Name<span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control autocomplete" placeholder="Enter user's lastname, firstname" name="user-name"/>
                        </div>
                    </div>
                    <div class="form-group collapse" id="groups">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Group's Name<span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control autocomplete" placeholder="Enter department's name" name="group-name"/>
                        </div>
                    </div>
                    <div class="form-group collapse" id="location">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Location<span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control autocomplete" placeholder="Enter room number for asset location" name="location"/>
                        </div>
                    </div>
                </div>
                {{-- <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" class="form-control collapse" placeholder="Enter the note for this asset" name="add-note" />
                      <button type="button" id="noteBtn" class="btn btn-default btn-sm">Add a Note</button>
                    </div>
                </div> --}}
                @if(! empty($editAsset))
                    @foreach($editAsset[0]->notes as $note)
                    <div class="notes form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                        <div class="entry col-md-9 col-sm-9 col-xs-12">
                            <div class="input-group">
                                <input class="form-control" name="add-note[]" type="text" placeholder="Enter a note for this asset." value="{{ $note->note }}"/>
                                <span class="input-group-btn">
                                    <button class="btn btn-remove btn-danger" type="button">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif

                <div class="notes form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                    <div class="entry col-md-9 col-sm-9 col-xs-12">
                        <div class="input-group">
                            <input class="form-control" name="add-note[]" type="text" placeholder="Enter a note for this asset." />
                            <span class="input-group-btn">
                                <button class="btn btn-success btn-add" type="button">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div>
                    </div>
            </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Did you update AIMs today?</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="aims-update">
                                <input type="checkbox" class="js-switch" name="aims-update"/> <label>No
                            </label>
                        </div>
                    </div>
                </div>
                <!-- form submission buttons -->
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <input type="hidden" name="isNew" value="1">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
                <!-- /form submission buttons -->
        </form>
    </div>
    <!-- /asset form content -->
</div>
</div>
@push('assets_scripts')
    <script type="text/javascript">
        $(function () {
          $('[name="asset-model"]').val('{{ $editAsset[0]->model_id }}');
          $('[name="nced-serial"]').val('{{ $editAsset[0]->nced_serial_number }}');
          $('[name="manufacturer-serial"]').val('{{ $editAsset[0]->ace_serial_number }}');
          $('#assetStatus').val('{{ $editAsset[0]->primary_status }}-{{ $editAsset[0]->secondary_status }}').change();

          if('{!! $editAsset[0]->owner_type !!}' === 'group') {
            $('#assignment').val('Group').change();
            $('[name="group-name"]').val('{{ $editAsset[0]->owner['short_name'] }}');
            $("#users :input").prop('required',false);
            $("#location :input").prop('required',false);
          }
          else if (('{{ $editAsset[0]->owner_type }}' === 'user') && ('{{ $editAsset[0]->owner['id'] }}' != 0)) {
            $('#assignment').val('User').change();
            $('[name="user-name"]').val('{{ $editAsset[0]->owner['ace_id'] }}');
            $("#groups :input").prop('required',false);
            $("#location :input").prop('required',false);
          }
          else if ('{!! $editAsset[0]->owner_type !!}' === 'location'){
            $('[name="location"]').val('{!! \App\Http\Libraries\Classes\Location::getRoomName(0) !!}');
            $("#groups :input").prop('required',false);
            $("#users :input").prop('required',false);
          }

        @if(! is_null($editAsset[0]->location_id))
          $('[name="location"]').val('{!! \App\Http\Libraries\Classes\Location::getRoomName(0) !!}');
        @endif
          $('[name="isNew"]').attr('name','isModified').val('assets_{{ $editAsset[0]->id }}');
        });
    </script>

@endpush
