@extends('it_assets_management.layout')

@section('assets_container')
    @if (empty($editAsset))
        @include('it_assets_management.add_new_asset')
    @else
        @include('it_assets_management.edit')
    @endif
@endsection

@push('assets_scripts')
    <script type="text/javascript">
    /*
    * Once NCED serial number is entered,
    * check if form is for new asset make
    * an ajax call to verify if same serial # exist.
    * if exists display error and disable submit btn
    * else remove error msg & enable submit
    */
    $('[name="nced-serial"]').change(function() {
      if($('[name="isNew"]').val() == '1') {
        AjaxGetCall('{{ url("it-assets/assets?term=nced_serial_number-") }}' +
        $('[name="nced-serial"]').val(),'json', true, function(response) {
        if (response != "Not Found" ) {
            $('#errorExist').remove();
            $('#ncedSerial :input').after("<small id='errorExist' class='red'>"
            +"Asset with same NCED serial number already exists in system. <a href='assets/"
            + response[0].id +"/edit'>Click here</a> to edit that asset.</small>");
            $('#assetForm [type="submit"]').prop("disabled", true);
        }
        else {
            $('#errorExist').remove();
            $('#assetForm [type="submit"]').prop("disabled", false);
        }
      });
      }
    });
    /*
    * Once Manudacturer serial number is entered,
    * check if form is for new asset make
    * an ajax call to verify if same serial # exist.
    * if exists display error and disable submit btn
    * else remove error msg & enable submit
    */
    $('[name="manufacturer-serial"]').change(function() {
      if($('[name="isNew"]').val() == '1') {
        AjaxGetCall('{{ url("it-assets/assets?term=ace_serial_number-") }}' +
        $('[name="manufacturer-serial"]').val(),'json', true, function(response) {
        if (response != "Not Found" ) {
            $('#manfErrorExist').remove();
            $('#manufacturerSerial :input').after("<small id='manfErrorExist' class='red'>"
            +"Asset with same manufacturer serial number already exists in system. <a href='assets/"
            + response[0].id +"/edit'>Click here</a> to edit that asset. If you know this is different asset, you can continue.</small>");
        }
        else {
            $('#manfErrorExist').remove();
        }
      });
      }
    });
    /*
    * Add notes on button click to the asset
    * Remove notes on red minus button click
    */
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();
        var controlForm = $('form#assetForm'),
            currentEntry = $(this).parents('.notes:first'),
            newEntry = $(currentEntry.clone()).insertAfter(currentEntry);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    })
    .on('click', '.btn-remove', function(e)
    {
        $(this).parents('.notes:first').remove();
        e.preventDefault();
        return false;
    });

    /*
    * Status selection: Primary and Secondary Status
    * are assigned automatically in controller.
    * In select-box value is assigned with: pri-sec id#
    */
    $('#assetStatus').change(function() {
        status = $(this).val();
         if (status=="2-7" || status=="3-8") {  // from db pri-sec id# of assetStatusOptions
             //show asset assignment form
             $('#assignmentForm, #location').show();
             $("#location :input").prop('required',true);
         }
        else {
            $('#assignmentForm').hide();
            $("#users :input, #groups :input, #location :input").prop('required',false);
        }
    });

    /*
    * Display input box according to selection
    * If user is selected, user input is displayed
    * If group is selected, group input is displayed
    */
    $('#assignment').change(function() {
        if($('#assignment').val() == "User") {
          $('#groups :input').val('');
          $("#groups :input, #location :input").prop('required',false);
          $('#groups, #location label span').hide();
          $('#users').show();
          $("#users :input").prop('required',true);
        }
        else if ($('#assignment').val() == "Group") { //if is group
              $('#users :input').val('');
              $("#users :input, #location :input").prop('required',false);
              $('#users, #location label span').hide();
              //change user search to group search
              $('#groups').show();
              $("#groups :input").prop('required',true);
        }
        else if ($('#assignment').val() == "Location") {
          $('#users :input, #groups :input').val('');
          $("#users :input, #groups :input").prop('required',false);
          $('#location label span').show();
          $('#users, #groups').hide();
          $("#location :input").prop('required',true);
        }
    });

    /*
    * Bind location data to input field
    * Make autocomplete using ajax call
    */
    $( "#location :input" ).autocomplete({
        minLength: 2,
        source: '{{ url("it-assets/location") }}',
        focus: function( event, ui ) {
          $( "#location :input" ).val( ui.item.facility_short_name
            +'-'+ui.item.building_short_name
            +'-'+ui.item.short_name );
          return false;
        },
        select: function( event, ui ) {
          $( "#location :input" ).val( ui.item.facility_short_name
            +'-'+ui.item.building_short_name
            +'-'+ui.item.short_name );

          return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
            // console.log(item.short_name);
        return $( "<li>" )
          .append( "<div>" + item.facility_short_name
            +'-'+ item.building_short_name
            +'-'+ item.short_name + "</div>" )
          .appendTo( ul );
    };      // end of location group data to input field

    /*
    * Bind group data to input field
    * Make autocomplete using ajax call
    */
    $( "#groups :input" ).autocomplete({
        minLength: 2,
        source: '{{ url("it-assets/groups") }}',
        focus: function( event, ui ) {
        $( "#groups :input" ).val( ui.item.short_name );
        return false;
        },
        select: function( event, ui ) {
        $( "#groups :input" ).val( ui.item.short_name );

        return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
        .append( "<div>" + item.short_name + "<br>" + item.long_name + "</div>" )
        .appendTo( ul );
    };    // end of bind group data to input field

    /*
    * Bind user data to input field
    * Make autocomplete using ajax call
    * If user has other asset create and dispay modal
    */
    $( "#users :input" ).autocomplete({
        minLength: 2,
        source: '{{ url("it-assets/users") }}',
        focus: function( event, ui ) {
            $( "#users :input" ).val( ui.item.ace_id );
            return false;
        },
        select: function( event, ui ) {
            $( "#users :input" ).val( ui.item.ace_id );
            $('#assetExist').remove();
            if (ui.item.has_asset.length >= 1) {
                $('#moveUserAssetForm').find('div').remove();
                ui.item.has_asset.forEach(function(element) {
                    // console.log(element.model.name);
                    let statusSelector = $('#assetStatus').parent().parent().clone();
                    // $(statusSelector).find("option[value='1-5']").remove(); // remove option to received-boxed
                    $(statusSelector).find("option[value='2-7']").remove(); // remove option to active-deploy
                    $(statusSelector).find("option[value='3-8']").remove(); // remove option to retired-repurposed
                    $(statusSelector).find("option[value='3-9']").remove(); // remove option to retired-return
                    $(statusSelector).find('label').text(element.model.name + '  #' + element.nced_serial_number);
                    let selectBox = $(statusSelector).find('select').parent().html();
                    let changedSelectBox = '<div class="input-group">'+ selectBox + '<span class="input-group-btn">'
                    + '<button type="button" data-loading-text="<i class=\'fa fa-spinner fa-spin pull-right\'></i> Updating..." id="'
                    + element.nced_serial_number
                    + '" class="btn btn-success pull-right" onclick="updateStatus(this.id)">Update</button></span></div>';
                    $(statusSelector).find('select').parent().html(changedSelectBox).after('<div class="clearfix"></div>');
                    $('#moveUserAssetForm').append(statusSelector);
                }); // end forEach

                if(ui.item.has_asset.length > 0) {
                    let textToShowInModal = ui.item.last_name + ', ' + ui.item.first_name + " has been assigned " + ui.item.has_asset.length
                            + " asset(s) in past. You can choose an option from dropdown to update status of the assets named on the left and click continue when you're done.";
                    $('#moveUserAssetForm').before('<p>'+textToShowInModal+'<br/></p>');
                }
                $('#moveUserAssetModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
            else {
            $('#moveUserAssetForm').find('div').remove();
            }
            return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.ace_id + "<br>" + item.last_name + ", " + item.first_name + "</div>" )
        .appendTo( ul );
    };    // end bind user data to input field

    // end of IT asset index functions
    </script>

@endpush
