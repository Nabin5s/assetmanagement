<!-- equipment type modal content -->
<div class="modal fade" id="addAssetTypeModal" tabindex="-1" role="dialog" aria-labelledby="Asset Types" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Add Asset Type</h5>
            </div>

            <div class="modal-body">
                <!-- new equipment type form is placed inside the body of modal -->
                <form id="assetTypesForm" action="{{ url('it-assets') }}" method="post" data-parsley-validate class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Asset Type <small>(eg. PC, Laptop, etc)</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-equipment-type" required="required"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /equipment type modal content -->
