<div id="deleteAsset">
    <form id="deleteAssetForm" action="{{ url('it-assets') }}" method="post">
      {{ csrf_field() }}
    <input type="hidden" name="isDeleted" value="1" /></form>
</div>
