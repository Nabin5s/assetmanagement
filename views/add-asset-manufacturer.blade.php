
<!-- ace version modal content -->
<div class="modal fade" id="addAssetManufacturerModal" tabindex="-1" role="dialog" aria-labelledby="Asset Manufacturers" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Add Asset Manufacturer</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="assetManufacturersForm" action="{{ url('it-assets') }}" method="post" data-parsley-validate class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Manufacturer Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-manufacturer-name" id="newManufacturerName" placeholder="Enter new manufacturer name" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ace version modal content -->
