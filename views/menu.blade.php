@if( request()->permission == "w")
<div class="">
    <div class="x_panel">
      <div class="x_content">
        @if(request()->path() != "assets")
          <a class="btn btn-app" href="{{ url('it-assets') }}">
            <i class="fa fa-plus"></i> Add Asset
          </a>
        @else
          <a class="btn btn-app" href="#">
            <i class="fa fa-plus blue"></i> Add Asset
          </a>
        @endif
        @if(request()->path() != "assets/all")
        <a class="btn btn-app" href="{{ url('it-assets/all') }}">
          <i class="fa fa-eye"></i> View All
        </a>
        @else
          <a class="btn btn-app" href="#">
            <i class="fa fa-eye blue"></i> View All
          </a>
        @endif
        @if(request()->path() != "assets/settings")
        <a class="btn btn-app" href="{{ url('it-assets/settings') }}">
          <i class="fa fa-gear"></i> Settings
        </a>
        @else
          <a class="btn btn-app" href="#">
            <i class="fa fa-gear blue"></i> Settings
          </a>
        @endif
      </div>
    </div>
  </div>
@endif
