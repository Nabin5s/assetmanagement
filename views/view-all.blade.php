@extends('it_assets_management.layout')
@section('assets_container')
            <div class="">
                  <div class="x_panel">
                        <div class="x_title">
                              <h2>View All IT Assets</h2>
                              <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- view all page content -->
                        <div class="x_content">
                              <div style='margin-top: 20px;' class='row' align="center">
                                    @include('templates.datatables-ajax')
                              </div>
                              @include('it_assets_management.delete-asset-form')
                        </div>
                        <!-- /view all page content -->
                  </div>
            </div>
      @endsection
      @push('assets_scripts')
      <script>
      // context menu functions
      function editAsset(data) {
        window.location = data.rowItem.ID+'/edit';
      }

      function aimsUpdate(data) {

        window.location = 'aims_' + data.rowItem.ID + '/edit';
      }

      function deleteAsset(data) {
        $('[name="isDeleted"]').val('assets_' + data.rowItem.ID );
          confirmAlert('deleteAssetForm', 'delete');

      }

      //For top 4 asset filters
      $( function() {
        table.search( "{{ $status }}" ).draw();
      });
      </script>
      @endpush
