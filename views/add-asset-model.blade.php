<!-- asset model modal content -->
<div class="modal fade" id="addAssetModelModal" tabindex="-1" role="dialog" aria-labelledby="Asset Models" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Add Asset Model</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="assetModelsForm" action="{{ url('it-assets') }}" method="post" data-parsley-validate class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Manufacturer</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" id="assetManufacturers" name="asset-manufacturer">
                                @foreach($assetManufacturers as $assetManufacturer)
                                        <option value="{{ $assetManufacturer->id }}">{{ $assetManufacturer->name }}</option>
                                @endforeach
                                <option value="Add New">Add New</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ACE Version</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" id="aceVersions" name="model-ace-version">
                                @foreach($aceVersions as $aceVersion)
                                <option value="{{ $aceVersion->id }}" >ACE {{ $aceVersion->version_number }}</option>
                                @endforeach
                                <option value="Add New">Add New</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Asset Type</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" id="assetTypes" name="model-equipment-type">
                                @foreach($assetTypes as $asset)
                                <option value="{{ $asset->id }}">{{ $asset->type }}</option>
                                @endforeach
                                <option value="Add New">Add New</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Display Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-model-name" required="required" />
                            <small>Name shown in Asset Model dropdown. Do not include ACE version here.</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Description</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-model-description" required="required" />
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Is this a computer model?</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="isComputer">
                                    <input type="checkbox" class="js-switch" name="is-computer" /> <label>No
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Memory Capacity</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" name="new-memory-capacity" required="required" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="cancel" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /asset model modal content -->
