<?php

namespace App\Http\Controllers\ITAssetsManagement;

use \App\Model\AssetsManagement as AssetModel;
use \App\Model\Notes;
use Carbon\Carbon;
use \App\Model\Auth\Users;
use \App\Model\Auth\Groups;
use App\Http\Libraries\Classes\Location;
class AssetsClass extends ITAssetsManagementController
{



  // public function __construct()
  // {
  //   $this->$assetStatusOptions = AssetModel\AssetStatusOptions::where('enabled', 1)->get();
  // }
  public static function getAssetStatusOptions($id)
  {
    return AssetModel\AssetStatusOptions::where('enabled', 1)->where('id', $id)->value('value');
  }
  /**
   * Check if any asset attribute exists in db not deleted
   * includes nced, manf, optional serial
   * @param  string  $type, int $value
   * @return object or string "Not Found"
   */
  protected static function getAttribute($type, $value)
  {
    $assets = AssetModel\Assets::where('deleted', 0)
    ->where($type, $value)
    ->get()->toArray();
    return ($assets == [] ? "Not Found" : $assets);
  }

  /**
   * Save asset to db.
   *
   * @param  request()
   * @return saved new asset's id
   */
  protected static function storeAsset()
  {
      $assets = new AssetModel\Assets;
      $assets->model_id = request()->get('asset-model');
      if(request()->get('asset-status') == "2-7" || request()->get('asset-status') == "3-8") { // asset is deployed or repurposed
        if(request()->get('assignment') == "User") {   // assigned to user
          $assets->owner_id = Users::where('ace_id', request()->get('user-name') )->value('id');
          $assets->owner_type = "user";   // use request()->get('assignment')
          if (request()->get('location') != "") { // assigned to user with location
              $assets->location_id = Location::getRoomID(request()->get('location'));
          }
        }
        elseif (request()->get('assignment') == "Group") { // assigned to group
          $assets->owner_id = \App\Model\Auth\Groups::where('short_name', request()->get('group-name'))->value('id');
          $assets->owner_type = "group";
          if (request()->get('location') != "") { // assigned to group with location
              $assets->location_id = Location::getRoomID(request()->get('location'));
          }
        }
        elseif (request()->get('assignment') == "Location") { // assigned to location only
            $assets->location_id = Location::getRoomID(request()->get('location'));
            $assets->owner_id = $assets->location_id;
            $assets->owner_type = "location";
        }
        else {
          return ; // error
        }
      }                             // end of asset deployed
      else {                        // not deployed, asset is staged or boxed
        $assets->owner_id = 0;                                 // system user
        $assets->owner_type = "user";
      }
      $assets->ace_serial_number = request()->get('manufacturer-serial');
      $assets->nced_serial_number = request()->get('nced-serial');

      if(request()->get('aims-update') == "1") {
          $assets->aims_updated = Carbon::now()->toDateTimeString();
      }
          //optional serial add here in future
      $assets->add_date = Carbon::now()->toDateTimeString();
      $assets->add_user = Users::where('ace_id', session()->get('ace_id'))->value('id');
      $assets->modified_date = Carbon::now()->toDateTimeString();
      $assets->deleted = 0;
      $status = explode('-', request()->get('asset-status'));
      $assets->primary_status = $status[0];
      $assets->secondary_status = $status[1];
      $assets->save();
      return $assets->id; // new asset id
  }

  /**
   * Handle request to modify existing asset
   *
   * @param  request()
   * @return \Illuminate\Http\Response
   */
  protected static function modifyAsset($id)
  {
    // find asset (in theory should always return object)
    $existingAsset = AssetsClass::getAttribute('id', $id);
    // delete asset
    AssetsClass::deleteAsset($id);
    //delete note
    AssetsClass::deleteNote($id);
    // save new asset, returns id
    // then save new note using asset id
    AssetsClass::storeNote(AssetsClass::storeAsset());
    // return
    return ;
  }

  /**
   * Handle request to delete existing asset
   * note gets deleted after asset
   * @param  request()
   * @return true on success
   */
  protected static function requestDeleteAsset($id) {
    // check if asset is assigned to anyone
    $status = AssetModel\Assets::where('id', $id)->value('secondary_status');
    // dd(AssetModel\Assets::where('id', $rowID)->value('secondary_status'));
    if($status == 7 || $status == 8) {
      return false; // asset is currently in deployed or repurposed
    }
    else {
      AssetsClass::deleteAsset($id);      // delete asset
      AssetsClass::deleteNote($id);      //delete note
    }
    return true;
  }
  /**
   * Handle request to modify existing asset
   *
   * @param  request()
   * @return \Illuminate\Http\Response
   */
  protected static function deleteAsset($id) {
    return AssetModel\Assets::where('id', $id)
    ->update([
      'deleted' => 1,                                   // delete existing
      'modified_date' => Carbon::now()->toDateTimeString(),
      'modified_user' => Users::where('ace_id', session()->get('ace_id'))->value('id')
    ]);
  }

  /**
   *
   *
   * @param  request()
   * @return \Illuminate\Http\Response
   */
  protected static function storeNote($assetId) {
    if (request()->has('add-note')) {
        foreach(request()->get('add-note') as $note) {
            if($note != "") {
                \DB::table('notes')
                ->insert([
                    'note' => $note,
                    'reply_id' => NULL, // 'System' user_id
                    'note_type' => 2,       // user note, refer noteTypes
                    'refer_id' => $assetId, // asset_id
                    'refer_table' => 1,     // assets table, refer noteTables
                    'add_date' => Carbon::now()->toDateTimeString(),
                    'add_user' => Users::where('ace_id', session()->get('ace_id') )->value('id'),
                    'deleted' => 0
                ]);
            }
        }
    }
    return ;
  }

  /**
   *
   *
   * @param  request()
   * @return \Illuminate\Http\Response
   */
  protected static function deleteNote($assetId) {
    return Notes::where(['refer_table' => 1, 'refer_id' => $assetId, 'deleted' => 0])
    ->update([
      'deleted' => 1,                // delete existing
    ]);
  }
  /**
   *
   *
   * @param  request()
   * @return \Illuminate\Http\Response
   */
  protected static function changeStatus($ncedSerial, $newStatus)
  {
    $status = explode('-', $newStatus);
    $item = AssetModel\Assets::where('nced_serial_number', $ncedSerial)->where('deleted', 0)->first();
    $newAsset = $item->replicate();
    AssetModel\Assets::where('nced_serial_number', $ncedSerial)->where('deleted', 0)->update([
      'deleted' => 1,                                   // delete existing
      'modified_date' => Carbon::now()->toDateTimeString(),
      'modified_user' => Users::where('ace_id', session()->get('ace_id'))->value('id')
    ]);
    $newAsset->owner_id = 0; // system
    $newAsset->owner_type = "user";
    $newAsset->aims_updated = NULL;
    $newAsset->add_date = Carbon::now()->toDateTimeString();
    $newAsset->add_user = Users::where('ace_id', session()->get('ace_id'))->value('id');
    $newAsset->primary_status = $status[0];
    $newAsset->secondary_status = $status[1];
    $newAsset->location_id = NULL;
    $newAsset->save();
  }

  protected static function assetReport()
  {
      // $new = new AssetsManagementController();
      // $all = (object) $new->getAll();      // get all assets

      $all = AssetModel\Assets::with(['owner', 'model', 'notes'])->where('deleted', 0)->orderBy('modified_date', 'desc')->get();
    //   dd( array_dot(json_decode($all, true)) );
      foreach ($all as $asset) {

        $assetArray = array_dot(json_decode($asset, true)); // break asset info into dot keys
        switch ($assetArray['owner_type']) {    // 'user', 'group', 'location'
          case 'user':
            if ($assetArray['owner_id'] === 0) {  // assigned to system (not active asset)
              $lastAssetModifier = Users::where('id', $assetArray['modified_user'] ?: $assetArray['add_user'])
                            ->pluck('last_name', 'first_name')->toArray();
              $assigned = '<i>' .array_first($lastAssetModifier) . ', ' . array_first(array_keys($lastAssetModifier)) .' (IS)</i>';
              $assignedTo = "IT Department Storage";
              $locationID = 318; // assign location of IT Dept
            }
            else {
              $assigned = $assetArray['owner.last_name'] . ', ' . $assetArray['owner.first_name'];
              $assignedTo = $assetArray['owner_type'];
              $locationID = $assetArray['location_id'] ?? Users::where('id', $assetArray['owner_id'] )->value('location'); // if no location assign location of user
            }
          break;
          case 'group':
            $assignedTo = $assetArray['owner_type'];
            $assigned = $assetArray['owner.long_name'];
            $locationID = $assetArray['location_id'] ?? Groups::with('manager')
              ->where('id', $assetArray['owner_id'])->first()->manager->location ?? 0; // if no location assigin location of group supervisor or 0
          break;
          case 'location':
            $assignedTo = $assetArray['owner_type'];
            $assigned = '<strike>Location Type?</strike>';
            $locationID = $assetArray['location_id'];
          break;
        }

        $location = Location::getRoomName( $locationID ?? 0 );

        if ( $locationID != $assetArray['location_id'] ) {
          // code generated location
          $location = '<i>' . $location . '</i>';
        }
        // dd(!is_null($assetArray['location_id'])?: 0);
// var_dump( Location::getRoomName( $assetArray['location_id'] ?? 0 ) );
        //check for notes
       //  dd($assetArray['notes.0.note'] ?: '');
      //  dd( ($assetArray['primary_status']-1)-1 );
        // dd ( AssetsClass::getAssetStatusOptions(( ($assetArray['primary_status']-1)-1 )) );
        //dd(array_dot(json_decode($each, true)));
        $apendNotes = '';
        foreach ($asset->notes as $note){
            $apendNotes = $apendNotes . $note->note . ';<br/>';
        }
        // dd($assetArray['modified_date'] ?? 'hey');
        $tableArray[] = [
          'ID' => $assetArray['id'],
          'NCED_Serial_#' => $assetArray['nced_serial_number'],
          'Manf_Serial_#' => $assetArray['ace_serial_number'],
          'Last_AIMS_Update' => !is_null($assetArray['aims_updated']) ? Carbon::parse($assetArray['aims_updated'])->format('m-d-Y') : "AIMS not updated yet",
          'Assigned_to' => $assignedTo, //$assetArray['owner_type'],
          'Assigned' => $assigned,
          'Primary_Status' => AssetsClass::getAssetStatusOptions($assetArray['primary_status']),
          'Secondary_Status' => AssetsClass::getAssetStatusOptions($assetArray['secondary_status']),
          'Notes' => $apendNotes,
          'Location' => $location, //!is_null(Location::getRoomName($assetArray['location_id'])) ?: ''
          'Last_Modified' => Carbon::parse($assetArray['modified_date'])->diffForHumans()
        ];
      } // end of foreach
      return  ["data" => $tableArray];
  }

  public static function getAssetCountOfStatus($status, $type = null)
  {
    $statusID = AssetModel\AssetStatusOptions::where('value', $status)->value('id');
    return AssetModel\Assets::where($type == "sec" ? 'secondary_status' : 'primary_status', $statusID)->where('deleted', '0')->count();
  }
}
