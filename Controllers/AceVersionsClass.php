<?php

namespace App\Http\Controllers\ITAssetsManagement;

use \App\Model\AssetsManagement as AssetModel;
use \App\Model\Notes;
use Carbon\Carbon;
use \App\Model\Auth\Users;
use App\Http\Libraries\Classes\Location;
class AceVersionsClass
{

  public static function storeAceVersion()
  {
    $aceVersions = new AssetModel\AceVersions;
    $aceVersions->version_number = request()->get('new-ace-version');
    $aceVersions->description = request()->get('new-ace-description');
    $aceVersions->add_date = Carbon::now()->toDateTimeString();
    $aceVersions->add_user = \Auth::user()->db()->id;
    $aceVersions->enabled = 1;
    $aceVersions->save();
    return $aceVersions->id;
  }

  public static function modifyAceVersion($id)
  {
      return AssetModel\AceVersions::where('id', $id)
      ->update([
        'version_number' => request()->get('new-ace-version'),
        'description' => request()->get('new-ace-description'),
        'modified_date' => Carbon::now()->toDateTimeString(),
        'modified_user' => \Auth::user()->db()->id,
      ]);
  }
}
