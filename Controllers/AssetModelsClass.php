<?php

namespace App\Http\Controllers\ITAssetsManagement;

use \App\Model\AssetsManagement as AssetModel;
use \App\Model\Notes;
use Carbon\Carbon;
use \App\Model\Auth\Users;
use App\Http\Libraries\Classes\Location;

class AssetModelsClass
{
  public static function storeAssetModel()
  {
    $assetModels = new AssetModel\AssetModels;
    if(request()->has('new-manufacturer-name')) {
      $assetModels->manufacturer_id = AssetManufacturersClass::storeAssetManufacturer();
    }
    else {
      $assetModels->manufacturer_id = request()->get('asset-manufacturer');
    }

    if(request()->has('new-ace-version')) {
        //store ace version then
      $assetModels->ace_version = AceVersionsClass::storeAceVersion();
    }
    else {
      $assetModels->ace_version = request()->get('model-ace-version');
    }

    if(request()->has('new-equipment-type')) {
        // store equipmentType
      $assetModels->asset_type = AssetTypesClass::storeAssetType();
    }
    else {
      $assetModels->asset_type = request()->get('model-equipment-type');
    }

    $assetModels->name = request()->get('new-model-name');
    $assetModels->description = request()->get('new-model-description');

    if(request()->get('is-computer') == null) {
      $assetModels->is_computer = 0;
    }
    else {
      $assetModels->is_computer = 1;
    }
    $assetModels->memory_capacity = request()->get('new-memory-capacity');
    $assetModels->add_date = Carbon::now()->toDateTimeString();
    $assetModels->add_user = \Auth::user()->db()->id;
    $assetModels->enabled = 1;
    $assetModels->save();
    return $assetModels->id;
  }

  public static function modifyAssetModel($id)
  {
      return AssetModel\AssetModels::where('id', $id)
      ->update([
        'manufacturer_id' => request()->get('asset-manufacturer'),
        'ace_version' => request()->get('model-ace-version'),
        'asset_type' => request()->get('model-equipment-type'),
        'name' => request()->get('new-model-name'),
        'description' => request()->get('new-model-description'),
        'modified_date' => Carbon::now()->toDateTimeString(),
        'modified_user' => \Auth::user()->db()->id,
      ]);
  }
}
