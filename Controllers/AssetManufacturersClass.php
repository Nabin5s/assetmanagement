<?php

namespace App\Http\Controllers\ITAssetsManagement;

use \App\Model\AssetsManagement as AssetModel;
use \App\Model\Notes;
use Carbon\Carbon;
use \App\Model\Auth\Users;
use App\Http\Libraries\Classes\Location;
class AssetManufacturersClass
{

  public static function storeAssetManufacturer()
  {
    $assetManufacturers = new AssetModel\AssetManufacturers;
    $assetManufacturers->name = request()->get('new-manufacturer-name');
    $assetManufacturers->add_date = Carbon::now()->toDateTimeString();
    $assetManufacturers->add_user = \Auth::user()->db()->id;
    $assetManufacturers->save();
    return $assetManufacturers->id;
  }
  public static function modifyAssetManufacturer($id)
  {
      return AssetModel\AssetManufacturers::where('id', $id)
      ->update([
        'name' => request()->get('new-manufacturer-name'),
        'modified_date' => Carbon::now()->toDateTimeString(),
        'modified_user' => \Auth::user()->db()->id,
      ]);
  }
}
