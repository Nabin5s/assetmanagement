<?php

namespace App\Http\Controllers\ITAssetsManagement;

use \App\Model\AssetsManagement as AssetModel;
use \App\Model\Notes;
use Carbon\Carbon;
use \App\Model\Auth\Users;
use App\Http\Libraries\Classes\Location;
class AssetTypesClass
{

      public static function storeAssetType()
      {
        $assetTypes = new AssetModel\AssetTypes;
        $assetTypes->type = request()->get('new-equipment-type');
        $assetTypes->add_date = Carbon::now()->toDateTimeString();
        $assetTypes->add_user = \Auth::user()->db()->id;
        $assetTypes->enabled = 1;
        $assetTypes->save();
        return $assetTypes->id;
      }

      public static function modifyAssetType($id)
      {
          return AssetModel\AssetTypes::where('id', $id)
          ->update([
            'type' => request()->get('new-equipment-type'),
            'modified_date' => Carbon::now()->toDateTimeString(),
            'modified_user' => \Auth::user()->db()->id,
          ]);
      }
}
