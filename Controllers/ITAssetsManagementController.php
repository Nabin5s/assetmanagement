<?php

namespace App\Http\Controllers\ITAssetsManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Model\AssetsManagement as AssetModel;

use Carbon\Carbon;
use \App\Model\Auth\Users;
use \App\Model\Auth\Groups;
use App\Http\Libraries\Classes\Location;
//Includes the Datatables class to set datatables settings.
use App\Http\Libraries\Classes\Datatables;
use App\Http\Libraries\Classes\TableLink;
use App\Http\Libraries\Classes\DTableBtn;
use App\Http\Libraries\Classes\ContextMenu;

class ITAssetsManagementController extends Controller
{

    protected $assets;

    public function __construct()
    {
        $this->middleware('auth');    // check user is authenticated
        $this->middleware('verify');    // verify if user has read or write permission
        $this->assets = AssetModel\Assets::where('deleted', 0)->get();   // load all assets
        $this->buildTopTiles(); // Makes a query to get all status and display tiles (depends on all assets)
        $this->time = Carbon::now()->toDateTimeString();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($this->assets);
        if( request()->permission == "r" )
        return view('it_assets_management.view-all',
         $this->getAll(), $this->allDataTable());       // Pass a view with the datatable's template
        else
          return view('it_assets_management.index')
          ->with([
              'assetModels' => $this->setUp('assetModels', 1, 'aceVersions'),
              'assetStatusOptions' => $this->setUp('AssetStatusOptions', 1),
              'assetManufacturers' => $this->setUp('AssetManufacturers', 1),
              'aceVersions' => $this->setUp('AceVersions', 1),
              'assetTypes' => $this->setUp('AssetTypes', 1),
            ]);
    }

    /**
     * Set Up tables related to asset management
     *
     * @return collection
     */
    protected function setUp($table, $state, $with = [])    // pass table_name, enabled/disabled (1/0) and with relations
    {
      $tableModel = '\App\Model\AssetsManagement\\'.$table;
    //   $tableCache = \Cache::remember($tableModel, 5, function() use ($tableModel, $state, $with) {
    //       return $tableModel::with($with)->where('enabled', $state)->get();
    //   });
      return $tableModel::with($with)->where('enabled', $state)->get();
    //   return $tableCache;
    }

    /**
     * Display a tile of count for various assets status
     *
     * @return View: it_assets_management/top-tiles.blade.php
     */
    protected function buildTopTiles()
    {
        $statusID = $this->setUp('AssetStatusOptions', 1); // get all the status options

        $count_each_status = $statusID->map(function($status) {
            $id = $status->id;
            $mode = ($status->status_mode == "sec" ? 'secondary_status' : 'primary_status');

            $each_status_assets_count[$status->value] = $this->assets->map(function($asset) use ($id, $mode) {
                return $asset->$mode == $id;
            })->sum();
            return $each_status_assets_count;
        });

        $result = array();

        foreach($count_each_status as $status_count) {
            $result[key($status_count).'Status'] = current($status_count);
        }
        // building menu with user permissions
        \View::composer('it_assets_management.top-tiles', function($view) use ($result) {   // Using laravel composer to add menu view
          $view->with($result);  // getting menu from Menu Model
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
    * This method is called when form post
    * method is used with 'assets' route
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if form was submitted to modify asset data
        if(request()->has('isModified')) {
          // Get table and row ID being modified
          $table = array_first(explode('_', request()->get('isModified')));
          $rowID = array_last(explode('_', request()->get('isModified')));
          // Pass request to one of the five tables
          switch ($table) {
            case 'assets':
              AssetsClass::modifyAsset($rowID);
              // redirect to view all page with success message
              session()->flash('message-type', 'success');
              session()->flash('message', 'Successfully modified selected entity!');
              return \Redirect::to('it-assets/all');
            break;
            case 'aceVersions':
            AceVersionsClass::modifyAceVersion($rowID);           // modify ace version
            break;
            case 'assetModels':
            AssetModelsClass::modifyAssetModel($rowID);
            break;
            case 'assetTypes':
            AssetTypesClass::modifyAssetType($rowID);
            break;
            case 'assetManufacturers':
            AssetManufacturersClass::modifyAssetManufacturer($rowID);
            break;
          }
          // redirect to settings page with success message
          session()->flash('message-type', 'success');
          session()->flash('message', 'Successfully modified selected entity!');
          return \Redirect::to('it-assets/settings');
        } // end of isModify Request
        // Check if isDeleted or isRestored
        elseif (request()->has('isDeleted') || request()->has('isRestore')){         // disable or enable row in db
          $data = request()->get('isDeleted') ?: request()->get('isRestore'); // assign data from one of 2 request variable.
          $table = array_first(explode('_', $data));
          $rowID = array_last(explode('_', $data));
          // check for foreign key depedencies
          switch ($table) {
            case 'assets':
            // asset can only be deleted (no restore)
            if ( AssetsClass::requestDeleteAsset($rowID) ) {
              return $this::returnSuccessTo('it-assets/all');            // redirect with success message
            }
            else {  // not deleted
              return $this::returnErrorTo('it-assets/all');            // redirect with error message
            }
            break;
            case 'aceVersions':
            case 'assetModels':
            case 'assetTypes':
            case 'assetManufacturers':
            // Determine to delete or restore
            $state = request()->has('isDeleted') ? 0 : 1;   // 0 = deleted
            $this->changeEntityState($table, $rowID, $state); // change enabled field value
            break;
          } // end of switch case

          return $this::returnSuccessTo('it-assets/settings');         // redirect with success message
        }
        // Check for changeStatusOf
        elseif (request()->has('changeStatusOf')) { // Check if isChangeOfStatus uses ajax POST
                  AssetsClass::changeStatus(request()->get('changeStatusOf'), request()->get('newStatus'));
                  return response (['success' => "Successfully updated Status"], 200);
        }
        // Check if isNew
        else {
          if( request()->has('nced-serial') && request()->has('isNew') ) {  // new asset db entry
            AssetsClass::storeNote(AssetsClass::storeAsset());
          }
          if( request()->has('new-model-name') ){    // new model entry (can have new manufacturer and/or ace ver and/or equip Type)
            AssetModelsClass::storeAssetModel();
          }
          elseif ( !request()->has('new-model-name') && request()->has('new-manufacturer-name') ) {
            AssetManufacturersClass::storeAssetManufacturer();
          }
          if(request()->has('new-ace-version')){           // new ace version db entry
            AceVersionsClass::storeAceVersion();
          }
          if(request()->has('new-equipment-type')){         // new equipment type db entry
            AssetTypesClass::storeAssetType();
          }
          // redirect with success message
          return $this::returnSuccessTo('it-assets');
        } // end of add new

        // Still here for some unseen reason?
        return $this::returnErrorTo('it-assets/all');
      } // end of Store

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $term = request()->query('term');   // query term from url?term=""
        switch ($id) {
          case 'groups':  // returns all groups for autocomplete search
              return Groups::where('long_name', 'like', '%'.$term.'%')
                  ->orWhere('short_name', 'like', '%'.$term.'%')
                  ->get();
              break;
          case 'users':   // returns all users for autocomplete search
              return Users::withAll()
                  ->where('first_name', 'like', '%'.$term.'%')
                  ->where('enabled', '=', '1')
                  ->orWhere('last_name', 'like', '%'.$term.'%')
                  ->orWhere('ace_id', 'like', '%'.$term.'%')
                  ->get();
              break;
          case 'location': // returns all location for autocomplete search
              return Location::getRoomsMatching($term);
              break;
          case 'all':   // returns view with all asset in Datatables
            return view('it_assets_management.view-all', $this->allDataTable($term));
            break;
          case 'assets':  // returns asset object if exists else 'not found'
              $attributes = explode("-", $term); // explode to attributeType, Value
              $assets = AssetsClass::getAttribute($attributes[0], $attributes[1]);
              return response()->json($assets); // To check if serial# already exists
              break;
          case 'settings':    // returns settings view
            if(request()->permission == "w") {
              return view('it_assets_management.settings',
                  $this->getAll() );
              }
              break;
          case 'json':    // returns json list of all assets
              return  AssetsClass::assetReport();
              break;
          case 'json-settings': // returns json data for each entity in settings
              $breakInput = explode("_", $term);
              $table = substr(array_first($breakInput), 0, -6);  // Remove 'Select' from key to get table name
              $arrayID = array_last($breakInput);

              return ([ $table => $this->getAll()[$table][$arrayID],
                  'dependent' => $this
                  ->dependsOn($table, $this->getAll()[$table][$arrayID]->id)]);
              break;
          default:
              return redirect()->back();
        }   // end of switch case
        return redirect()->back();
     } // end of show action

      protected function dependsOn($table, $rowID)
      {
          $table =  preg_replace('/Inactive$/', '', $table); // remove inactive from table name
        switch ($table) {
          case 'aceVersions':
             return AssetModel\AssetModels::where('enabled', 1)->where('ace_version', $rowID)->count() . ' Asset Models';
          break;
          case 'assetModels':
             return AssetModel\Assets::where('deleted', 0)->where('model_id', $rowID)->count() . ' Assets';
          break;
          case 'assetTypes':
             return AssetModel\AssetModels::where('enabled', 1)->where('asset_type', $rowID)->count() . ' Asset Models';
          break;
          case 'assetManufacturers':
             return AssetModel\AssetModels::where('enabled', 1)->where('manufacturer_id', $rowID)->count() . ' Asset Models';
          break;
        }
      }

    protected function allDataTable($term = null)
    {
        $tableSettings = new Datatables();    // new instance of Datatables
        $tableSettings->headers = [
         'ID',
         'NCED_Serial_#',
         'Manf_Serial_#',
         'Last_AIMS_Update',
         'Assigned_to',
         'Assigned',
         'Primary_Status',
         'Secondary_Status',
         'Notes',
         'Location',
         'Last_Modified'
        ];
        // $tableSettings->collection = (object) $tableArray;      // object for "view all" table
        //$link = new TableLink("aims?term={ID}","ID","AIMS update","fa fa-arrow-circle-o-up");
        //$tableSettings->links = [$link];
        if( \Auth::user()->hasPermission() != "r" ) {
        $tableSettings->contextMenuOptions = [                  // right click menu options
         new ContextMenu("Edit Asset", "fa-edit","editAsset"),
         new ContextMenu("Update AIMS", "fa-arrow-circle-o-up","aimsUpdate"),
         // new ContextMenu("Asset History", "fa-retweet","assetHistory"),
         new ContextMenu("Delete Asset", "fa-trash","deleteAsset")
        ];
        }
        $tableSettings->defaultButtons = DTableBtn::GetAllBtns( // get all buttons except passed
        DTableBtn::PageLength
        );
        //  $tableSettings->excludeExportColumn = ["AIMS update"];
        $tableSettings->hideColumn=[0];
        $tableSettings->orderBy = [0,"desc"];         // When a user clicks, on a export button, the column Mask will be excluded.
        $tableSettings->dataSourceUrl = url('it-assets/json');
        $tableSettings->height = '350px';
        return ["tableSettings" => $tableSettings, 'status' => $term];       // Pass a view with the datatable's template
     }
         /**
          * Show the form for editing the specified resource.
          *
          * @param  int  $id
          * @return \Illuminate\Http\Response
          */
         public function edit($id)
         {
           // dd(array_first(explode("_", $id)));
           if(array_first(explode("_", $id)) == "aims") {
             return $this->updateAims(array_last(explode("_", $id)));
           }
             // edit asset by updating old one as deleted
             $asset = AssetModel\Assets::with(['owner', 'model', 'notes'])->where('deleted', 0)->where('id', $id)->get();
             if ($asset == NULL) {
               session()->flash('message-type', 'error');
               session()->flash('message', 'Asset not found!!!');
               return \Redirect::to('it-assets/all');
             }
             else {
               // dd($id);
               return view('it_assets_management.index')
               ->with([
                   'assetModels' => $this->setUp('assetModels', 1, 'aceVersions'),
                   'assetStatusOptions' => $this->setUp('AssetStatusOptions', 1),
                   'assetManufacturers' => $this->setUp('AssetManufacturers', 1),
                   'aceVersions' => $this->setUp('AceVersions', 1),
                   'assetTypes' => $this->setUp('AssetTypes', 1),
                 ])
                 ->with('editAsset', $asset);
             }
         }

         public function updateAims($id)
         {
               $asset = AssetModel\Assets::find($id);
               $asset->aims_updated = $this->time;
               $asset->save();
               // redirect with success message
               session()->flash('message-type', 'success');
               session()->flash('message', 'Successfully updated AIMS date of Asset!');
               // return response("hello", 200);
               return \Redirect::to('it-assets/all');
         }

         /**
          * These are some helper methods
          * to get the dependent data
          * @return array with various data.
          */
          protected function getAll()
          {
            return ['assetModels' => $this->setUp('AssetModels', 1),
                   'assetTypes' => $this->setUp('AssetTypes', 1),
                   'aceVersions' => $this->setUp('AceVersions', 1),
                   'assetManufacturers' => $this->setUp('AssetManufacturers', 1),
                   'assetStatusOptions' => AssetModel\AssetStatusOptions::where('enabled', 1)->get(),
                   'assetModelsInactive' => $this->setUp('AssetModels', 0, ['aceVersions']),
                   'assetTypesInactive' => $this->setUp('AssetTypes', 0),
                   'aceVersionsInactive' => $this->setUp('AceVersions', 0),
                   'assetManufacturersInactive' => $this->setUp('AssetManufacturers', 0),
                 ];
          }
          protected function changeEntityState($table, $row, $state) {
            $tableModel = '\App\Model\AssetsManagement\\'.$table;
            return $tableModel::where('id', $row)
            ->update([
              'enabled' => $state,                                   // delete existing
              'modified_date' => $this->time,
              'modified_user' => Users::where('ace_id', session()->get('ace_id'))->value('id'),
            ]);
          }
          protected static function returnErrorTo($url)
          {
            // redirect with error message
            session()->flash('message-type', 'error');
            session()->flash('message', 'Other entities depend on this data.');
            return \Redirect::to($url);
          }
          protected static function returnSuccessTo($url)
          {
            // redirect with success message
            session()->flash('message-type', 'success');
            session()->flash('message', 'Successfully made changes to Assets!');
            return \Redirect::to($url);
          }
     }
